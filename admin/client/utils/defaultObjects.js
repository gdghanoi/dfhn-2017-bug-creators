const NEW_TABLE = {
  uid: '',
  nSeats: 0,
  floor: 0,
  description: '',
}

const NEW_MENU = {
  price: 0,
  image: '',
  name: '',
  description: '',
}

export {
  NEW_TABLE,
  NEW_MENU,
}
