import Vue from 'vue'
import Router from 'vue-router'

import Index from '../pages/Index'
import Login from '../pages/Login'
import TableList from '../pages/TableList'
import OrderList from '../pages/OrderList'
import History from '../pages/History'
import MenuList from '../pages/MenuList'

import {
  PAGE_LOGIN,
  PAGE_INDEX,
  PAGE_TABLE_LIST,
  PAGE_ORDER_LIST,
  PAGE_HISTORY,
  PAGE_MENU_LIST,
} from './routeNames'

Vue.use(Router)

const routes = [
  {
    path: '/login',
    name: PAGE_LOGIN,
    component: Login,
  },
  {
    path: '/',
    name: PAGE_INDEX,
    component: Index,
  },
  {
    path: '/tables',
    name: PAGE_TABLE_LIST,
    component: TableList,
  },
  {
    path: '/menus',
    name: PAGE_MENU_LIST,
    component: MenuList,
  },
  {
    path: '/orders',
    name: PAGE_ORDER_LIST,
    component: OrderList,
  },
  {
    path: '/history',
    name: PAGE_HISTORY,
    component: History,
  },
]

// routes.push({
//   path: '*',
//   redirect: '/login',
// })

const router = new Router({
  routes,
  mode: 'history',
})

export default router
