/* eslint-disable import/prefer-default-export */

const PAGE_INDEX = 'Index'
const PAGE_LOGIN = 'Login'
const PAGE_TABLE_LIST = 'Tables'
const PAGE_MENU_LIST = 'Menus'
const PAGE_ORDER_LIST = 'Orders'
const PAGE_HISTORY = 'History'

export {
  PAGE_INDEX,
  PAGE_LOGIN,
  PAGE_TABLE_LIST,
  PAGE_MENU_LIST,
  PAGE_ORDER_LIST,
  PAGE_HISTORY,
}
