import Vue from 'vue'
// import GoogleAuth from 'google-auth-library'
import Cookies from 'js-cookie'

import {
  MUTATION_SET_USER,
  MUTATION_SET_IS_AUTHENTICATED,
  MUTATION_SET_USER_TOKEN,
} from '../mutations'

import {
  ACTION_LOGIN,
  ACTION_GET_USER,
} from '../actions'

const userModule = {
  state: {
    user: {
      uid: '',
      name: '',
      email: '',
    },

    isAuthenticated: false,
    token: '',

    // user: {
    //   uid: 'U5xpCGxyB5SRLoa7XqlD4IMZOlO2',
    //   name: 'Bang Nguyen',
    //   email: 'bangcht@gmail.com',
    // },

    // isAuthenticated: true,
    /* eslint-disable */
    // token: 'ya29.GlsIBfV5PnIUxi5lSR4AbLM9EXAee9g2OONemrlD85iMLXT9o5BNHPIUIqoVme52VVsa_15Fg3CF1GEWZPaKue_q8MHqWTTIE1nSPt7itMAMHNdN1xhvRKWZhkpj',
  },

  mutations: {
    [MUTATION_SET_USER](state, user) {
      Vue.set(state, 'user', user)
    },

    [MUTATION_SET_USER_TOKEN](state, token) {
      Vue.set(state, 'token', token)
    },

    [MUTATION_SET_IS_AUTHENTICATED](state, isAuthenticated) {
      Vue.set(state, 'isAuthenticated', isAuthenticated)
    },
  },

  actions: {
    [ACTION_LOGIN]({ commit }, { user, token }) {
      commit(MUTATION_SET_USER, user)
      commit(MUTATION_SET_IS_AUTHENTICATED, true)
      commit(MUTATION_SET_USER_TOKEN, token)

      Cookies.set('r_t', token, { expires: 3 })
    },

    [ACTION_GET_USER]() {
      // const CLIENT_ID = 'asdjlkadjslksad'
      // const auth = new GoogleAuth()
      // const client = new auth.OAuth2(CLIENT_ID, '', '')
      // client.verifyIdToken(
      //   state.token,
      //   CLIENT_ID,
      //   (e, login) => {
      //     const payload = login.getPayload()
      //     // const userid = payload['sub']
      //     console.log(payload)
      //   },
      // )
    },
  },
}

export default userModule
