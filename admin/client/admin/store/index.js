import Vue from 'vue'
import Vuex from 'vuex'

import userModule from './userModule'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user: userModule,
  },

  strict: process.env.NODE_ENV !== 'production',
})

export default store
