/* eslint-disable import/prefer-default-export */

const ACTION_LOGIN = 'LOGIN'
const ACTION_LOGOUT = 'LOGOUT'
const ACTION_GET_USER = 'GET_USER'
const ACTION_UPDATE_TABLE = 'UPDATE_TABLE'

export {
  ACTION_LOGIN,
  ACTION_LOGOUT,
  ACTION_GET_USER,
  ACTION_UPDATE_TABLE,
}
