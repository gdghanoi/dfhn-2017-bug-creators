import store from '@/admin/store'

import { ACTION_GET_USER } from '@/admin/store/actions'
import { PAGE_LOGIN } from '@/admin/router/routeNames'

export default {
  beforeRouteEnter(to, from, next) {
    if (to.name === PAGE_LOGIN || store.state.userModule.isAuthenticated) {
      next()
    } else {
      store.dispatch(ACTION_GET_USER)
        .then(() => {
          if (store.state.userModule.isAuthenticated) {
            next()
          } else {
            next({ name: PAGE_LOGIN })
          }
        }, () => {
          next({ name: PAGE_LOGIN })
        })
    }
  },
}
