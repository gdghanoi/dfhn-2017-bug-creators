module.exports = {
  'root': true,
  'extends': [
    'vue',
    'airbnb',
  ],
  'env': {
    'browser': true,
  },
  'globals': {
  },
  'parser': 'babel-eslint',
  'rules': {
    'semi': [2, 'never'],
    'no-console': 'warn',
    'no-debugger': 'warn',
    'complexity': [1, 11],
    'no-empty': ['error', { 'allowEmptyCatch': true }],
    'no-underscore-dangle': ['error', { 'allow': ['_id'] }],
  },
  'plugins': [
    'import',
    'xss',
    'promise',
    'vue',
    'html',
  ],
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js',
      },
    },
    'html/report-bad-indent': 2,
    'html/indent': '+2',
  },
}
