import 'babel-polyfill'

import Vue from 'vue'
import Element from 'element-ui'
import firebase from 'firebase'
import Cookies from 'js-cookie'

import { MUTATION_SET_USER_TOKEN } from '@/admin/store/mutations'

import App from './admin/App.vue'
import router from './admin/router'
import store from './admin/store'
// import userAccess from './admin/mixins/userAccess'

import './styles'
import 'element-ui/lib/theme-chalk/index.css' // eslint-disable-line import/first

// Set authorization token
const token = Cookies.get('r_t') || ''
store.commit(MUTATION_SET_USER_TOKEN, token)

// Vue.mixin(userAccess)

const config = {
  apiKey: 'AIzaSyC8dBkYzksjtgDGzhboe6fZxvlFRBDmjs4',
  authDomain: 'bug-creators.firebaseapp.com',
  databaseURL: 'https://bug-creators.firebaseio.com',
  projectId: 'bug-creators',
  storageBucket: 'bug-creators.appspot.com',
  messagingSenderId: '762727484613',
}

firebase.initializeApp(config)

Vue.use(Element)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  ...App,
  router,
  store,
}).$mount('#app')
