/* eslint-disable */
const path = require('path')

module.exports = (config) => ({
  map: config.env !== 'production',
  plugins: [
    require('postcss-import')({
      path: path.resolve(__dirname, 'client/style'),
    }),
    require('postcss-utilities'),
    require('rucksack-css'),
    require('postcss-cssnext'),
    require('postcss-short'),
    require('postcss-calc'),
    require('precss')({
      path: path.resolve(__dirname, 'client/style'),
    }),
    require('css-mqpacker'),
    require('postcss-browser-reporter'),
  ],
})
