module.exports = {
  'root': true,
  'plugins': ['node', 'jest'],
  'extends': [
    'standard',
    'eslint:recommended',
    'plugin:node/recommended',
    'plugin:jest/recommended',
    'airbnb-base',
  ],
  'env': {
    'jest/globals': true,
  },
  'rules': {
    'node/exports-style': ['error', 'module.exports'],
    'comma-dangle': ['error', 'always-multiline'],
    'semi': ['error', 'never'],
    'no-underscore-dangle': ['error', { 'allow': ['_id'] }],
    'no-console': 'warn',
    'no-debugger': 'warn',
    'complexity': [1, 11],
    'no-empty': ['error', { 'allowEmptyCatch': true }],
  },
}
