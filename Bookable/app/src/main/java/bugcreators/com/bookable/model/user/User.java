package bugcreators.com.bookable.model.user;

import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Dandoh on 11/18/17.
 */

public class User {
    String uuid;
    String email;
    String name;

    public static User fromFirebaseUser(FirebaseUser firebaseUser) {
        return new User(firebaseUser.getUid(),
                firebaseUser.getEmail(), firebaseUser.getDisplayName());
    }

    public User(String uuid, String email, String name) {
        this.uuid = uuid;
        this.email = email;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
