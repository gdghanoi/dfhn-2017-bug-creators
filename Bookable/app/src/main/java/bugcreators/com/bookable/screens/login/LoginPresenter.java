package bugcreators.com.bookable.screens.login;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.model.user.User;
import bugcreators.com.bookable.model.user.UserRepository;

/**
 * Created by Dandoh on 4/9/16.
 */
public class LoginPresenter implements LoginContract.UserActionListener {


    private static final int RC_SIGN_IN = 12;
    private static final String TAG = LoginPresenter.class.getCanonicalName();

    private LoginContract.View mView;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private Activity mActivity;


    LoginPresenter(Activity activity, LoginContract.View view) {
        mView = view;
        mActivity = activity;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id)).requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void loginGoogle() {
        mView.showProcessingIndicator(true);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mView.redirect(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "Den day roi result");
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
//                mView.showProcessingIndicator(false);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                mView.loginFailure("Error logging in");
                mView.showProcessingIndicator(false);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getEmail());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            UserRepository
                                    .getInstance()
                                    .insertUser(User.fromFirebaseUser(user));
                            mView.loginSuccess(user);
                            Log.d(TAG, "Den day roi" + user.getEmail());
                            Log.d(TAG, "Den day roi" + user.getDisplayName());
                            Log.d(TAG, "Den day roi" + user.getUid());
                            mView.showProcessingIndicator(false);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            mView.loginFailure("Invalid login credential");
                            mView.showProcessingIndicator(false);
                        }
                        //justapass

                    }
                });
    }
}
