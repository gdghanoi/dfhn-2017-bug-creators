package bugcreators.com.bookable.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dandoh on 11/18/17.
 */

public class Order implements Parcelable {
    String date = "";
    Map<String, Integer> dishes = new HashMap<>();
    boolean isDinner;
    String tableId = "";
    String time = "";
    String userId = "";



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Integer> getDishes() {
        return dishes;
    }

    public void setDishes(Map<String, Integer> dishes) {
        this.dishes = dishes;
    }

    public boolean isDinner() {
        return isDinner;
    }

    public boolean getIsDinner() {
        return isDinner;
    }

    public void setIsDinner(boolean dinner) {
        isDinner = dinner;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeInt(this.dishes.size());
        for (Map.Entry<String, Integer> entry : this.dishes.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeValue(entry.getValue());
        }
        dest.writeByte(this.isDinner ? (byte) 1 : (byte) 0);
        dest.writeString(this.tableId);
        dest.writeString(this.time);
        dest.writeString(this.userId);
    }

    public Order() {
    }

    protected Order(Parcel in) {
        this.date = in.readString();
        int dishesSize = in.readInt();
        this.dishes = new HashMap<String, Integer>(dishesSize);
        for (int i = 0; i < dishesSize; i++) {
            String key = in.readString();
            Integer value = (Integer) in.readValue(Integer.class.getClassLoader());
            this.dishes.put(key, value);
        }
        this.isDinner = in.readByte() != 0;
        this.tableId = in.readString();
        this.time = in.readString();
        this.userId = in.readString();
    }

    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
