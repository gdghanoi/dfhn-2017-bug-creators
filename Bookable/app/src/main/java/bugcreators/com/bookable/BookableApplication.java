package bugcreators.com.bookable;

import android.app.Application;
import android.content.Context;

public class BookableApplication extends Application{

    private static final String TAG = "BookableApplication";
    private static Context mApplicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationContext = this;

    }

    public static Context getAppContext() {
        return mApplicationContext;
    }

}
