package bugcreators.com.bookable.screens.dishes;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.model.Table.Table;
import bugcreators.com.bookable.model.dish.Dish;
import bugcreators.com.bookable.model.order.Order;

/**
 * Created by AnhVu on 11/18/17.
 */

public class DishPresenter implements DishContract.UserActionListener {

    private DishContract.View mView;
    private Activity mActivity;
    DatabaseReference mDatabaseReference;

    private Map<String, Dish> mDishes;
    ValueEventListener mDishesEventListener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i("HAHA", "ko loi");
            mDishes = new HashMap<>();
            for (DataSnapshot ds : dataSnapshot.getChildren()) {

                Dish dish = ds.getValue(Dish.class);
                mDishes.put(ds.getKey(), dish);

            }
            mView.showProcessingIndicator(false);
            mView.displayDishList(new ArrayList<Dish>(mDishes.values()));
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.i("HAHA", "loi");
            mView.showProcessingIndicator(false);
        }
    };


    DishPresenter(Activity activity, DishContract.View view) {
        mView = view;
        mActivity = activity;
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void syncData() {
        mView.showProcessingIndicator(true);
        mDatabaseReference.child("dishes").addValueEventListener(mDishesEventListener);
    }

    @Override
    public void stopSyncData() {
        mDatabaseReference.child("dishes").removeEventListener(mDishesEventListener);
    }

    @Override
    public void insertOrder(Order order) {
        DatabaseReference newNode = mDatabaseReference.child("orders").push();
        newNode.setValue(order);
        mView.afterInsert();
    }
}
