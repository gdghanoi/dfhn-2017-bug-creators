package bugcreators.com.bookable.screens.time;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericRetainedFragment;
import bugcreators.com.bookable.model.order.Order;
import bugcreators.com.bookable.screens.tables.TablesActivity;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by AnhVu on 11/18/17.
 */

public class TimeFragment extends GenericRetainedFragment implements TimeContract.View {

    private TimeContract.UserActionListener mUserActionListener;
    @BindView(R.id.tv_date)
    TextView mDateTextView;

    @BindView(R.id.tv_time)
    TextView mTimeTextView;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private boolean isLunchSelected = true;

    public static Fragment getInstance() {
        return new TimeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserActionListener = new TimePresenter(this.getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "on create view");
        return inflater.inflate(R.layout.fragment_time, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.btn_date)
    void onDateClick() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mDateTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_time)
    void onTimeClick() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

//        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this.getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        mTimeTextView.setText(hourOfDay + ":" + minute);
                        mHour = hourOfDay;
                        mMinute = minute;
                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }

    @OnClick(R.id.radio_lunch)
    void onLunchClick() {
        isLunchSelected = true;
    }
    @OnClick(R.id.radio_dinner)
    void onDinnerClick() {
        isLunchSelected = false;
    }

    @OnClick(R.id.btn_continue)
    void onContinueClick() {
        Log.d(TAG, mDay + " " + mMonth + " " + mYear + " " + mHour + " " + mMinute + " " + isLunchSelected);

        Order order = new Order();
        order.setDate(mYear + "-" + (mMonth + 1) + "-" + mDay);
        order.setTime(mHour + ":" + mMinute);
        order.setIsDinner(!isLunchSelected);

        Intent i = TablesActivity.makeIntent(getActivity(), order);
        startActivity(i);
    }

}
