package bugcreators.com.bookable.screens.tables;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bugcreators.com.bookable.model.Table.Table;
import bugcreators.com.bookable.model.order.Order;

/**
 * Created by Dandoh on 4/9/16.
 */
public class TablesPresenter implements TablesContract.UserActionListener {


    private static final String TAG = TablesPresenter.class.getCanonicalName();
    private Map<String, Table> mTables;
    private Map<String, Order> mOrders;
    private Order mOrderInfo;
    private boolean hasTable = false;
    private boolean hasOrder = false;
    private TablesContract.View mView;
    private Activity mActivity;
    DatabaseReference mDatabaseReference;


    ValueEventListener mOrdersEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "lay ve");
            mOrders = new HashMap<>();
            for (DataSnapshot ds : dataSnapshot.getChildren()) {

                Order order = ds.getValue(Order.class);
                Log.i(TAG, order.isDinner() + "");
                mOrders.put(ds.getKey(), order);

            }
            hasOrder = true;
            if (hasTable) {
                mView.showProcessingIndicator(false);
                computeAvailability();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

            mView.showProcessingIndicator(false);
        }
    };


    ValueEventListener mTablesEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            mTables = new HashMap<>();
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                Table table = ds.getValue(Table.class);
                mTables.put(ds.getKey(), table);
            }
            hasTable = true;
            if (hasOrder) {
                mView.showProcessingIndicator(false);
                computeAvailability();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            mView.showProcessingIndicator(false);
        }
    };



    TablesPresenter(Activity activity, TablesContract.View view) {
        mView = view;
        mActivity = activity;
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mOrders = new HashMap<>();
    }

    @Override
    public void syncData(Order orderInfo) {
        mView.showProcessingIndicator(true);
        mOrderInfo = orderInfo;
        mDatabaseReference.child("orders").addValueEventListener(mOrdersEventListener);
        mDatabaseReference.child("tables").addValueEventListener(mTablesEventListener);
    }

    @Override
    public void stopSyncData() {
        mDatabaseReference.child("orders").removeEventListener(mOrdersEventListener);
        mDatabaseReference.child("tables").removeEventListener(mTablesEventListener);
    }

    private void computeAvailability() {
        Log.i(TAG, "Den day ne");
        List<Table> tables = new ArrayList<>(mTables.values());
        List<Order> orders = new ArrayList<>(mOrders.values());
        for (Order order: orders) {
            Log.i(TAG, order.isDinner() + " ");
            Log.i(TAG, order.getDate() + " ");
            Log.i(TAG, order.getTableId() + " ");
        }
        List<Table> availableTables = new ArrayList<>();
        for (Table table : tables) {
            table.setAvailable(true);
            for (Order order : orders) {
                if (order.getDate().equals(mOrderInfo.getDate()) &&
                        order.isDinner() == mOrderInfo.isDinner()) {
                    if (order.getTableId().equals(table.getId())) {
                        table.setAvailable(false);
                    }
                }
            }
            availableTables.add(table);

        }
        mView.displayTableAvailability(availableTables);
    }


}
