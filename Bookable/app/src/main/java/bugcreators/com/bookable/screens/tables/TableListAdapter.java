package bugcreators.com.bookable.screens.tables;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.model.Table.Table;

/**
 * Created by AnhVu on 4/9/16.
 */
public class TableListAdapter
        extends RecyclerView.Adapter<TableListAdapter.ViewHolder> {

    private static final String TAG = "TableListAdapter";
    private final OnTableClickListener mOnTableClickListener;

    static interface OnTableClickListener {
        void onTableClick(Table table);
    }

    private List<Table> mTables;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TableListAdapter(OnTableClickListener onTableClickListener) {
        this.mOnTableClickListener = onTableClickListener;
        mTables = new ArrayList<>();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
//         create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_table, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Table table = mTables.get(position);

        View v = holder.itemView;
        TextView name = (TextView) v.findViewById(R.id.text_table_name);

        if (table.isAvailable()) {
            name.setText(String.format("%d", table.getNumber()));
        } else {
            name.setText(String.format("%d (Taken)", table.getNumber()));
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnTableClickListener.onTableClick(table);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mTables.size();
    }


    public void setTableList(List<Table> tables) {
        this.mTables = tables;
        notifyDataSetChanged();
    }

}