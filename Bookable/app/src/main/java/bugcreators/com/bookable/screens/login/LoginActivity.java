package bugcreators.com.bookable.screens.login;

import android.os.Bundle;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericActivity;


public class LoginActivity extends GenericActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        if (savedInstanceState == null)
            initFragment(LoginFragment.getInstace());
    }
}
