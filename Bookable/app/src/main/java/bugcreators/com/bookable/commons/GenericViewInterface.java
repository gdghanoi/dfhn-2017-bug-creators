package bugcreators.com.bookable.commons;

import android.content.Intent;

/**
 * Created by Dandoh on 3/23/16.
 */
public interface GenericViewInterface {
    void showProcessingIndicator(boolean isProcessing);
    void redirect(Intent intent, int code);
}
