package bugcreators.com.bookable.screens.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericRetainedFragment;
import bugcreators.com.bookable.screens.time.TimeActivity;
import butterknife.OnClick;

/**
 * Created by Dandoh on 4/9/16.
 */
public class LoginFragment extends GenericRetainedFragment implements LoginContract.View {


    private LoginContract.UserActionListener mUserActionListener;

    public static Fragment getInstace() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserActionListener = new LoginPresenter(this.getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.button_google_login)
    void logInGoogle() {
        mUserActionListener.loginGoogle();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mUserActionListener.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void loginSuccess(FirebaseUser user) {
        Log.d(TAG, "on login succeeded");
        startActivity(TimeActivity.getIntent(this.getActivity()));
    }

    @Override
    public void loginFailure(String message) {

    }


}
