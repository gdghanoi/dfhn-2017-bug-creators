package bugcreators.com.bookable.commons;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public class GenericRetainedFragment extends GenericFragment {

    private ProgressDialog mProgressDialog;
    protected boolean isProcessing;
    protected Unbinder mUnbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
        mProgressDialog = new ProgressDialog(getActivity());

        if (isProcessing) {
            showLoading();
        }

    }

    @Override
    public void onDestroyView() {


        super.onDestroyView();
        mProgressDialog = null;
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected void showLoading() {

        isProcessing = true;
        if (mProgressDialog == null || mProgressDialog.isShowing())
            return;

        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait ...");
        mProgressDialog.show();
    }

    protected void dismissLoading() {
        isProcessing = false;
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public void showProcessingIndicator(boolean isProcessing) {

        if (isProcessing) {
            showLoading();
        } else {
            dismissLoading();
        }
    }

    public void redirect(Intent intent, int code) {
        startActivityForResult(intent, code);
    }


}
