package bugcreators.com.bookable.screens.tables;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericRetainedFragment;
import bugcreators.com.bookable.commons.GenericRetainedToolbarFragment;
import bugcreators.com.bookable.customview.GridSpacingItemDecoration;
import bugcreators.com.bookable.model.Table.Table;
import bugcreators.com.bookable.model.dish.Dish;
import bugcreators.com.bookable.model.order.Order;
import bugcreators.com.bookable.screens.dishes.DishActivity;
import bugcreators.com.bookable.screens.login.LoginContract;
import bugcreators.com.bookable.screens.login.LoginPresenter;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Dandoh on 4/9/16.
 */
public class TablesFragment extends GenericRetainedToolbarFragment implements TablesContract.View,
        TableListAdapter.OnTableClickListener{

    private static final String ORDER_KEY = "Order key";
    private TableListAdapter mTableListAdapter;
    private TablesContract.UserActionListener mUserActionListener;

    @BindView(R.id.list_table)
    RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    public static Fragment getInstance(Order order) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ORDER_KEY, order);
        Fragment fragment = new TablesFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserActionListener = new TablesPresenter(this.getActivity(), this);
        mTableListAdapter = new TableListAdapter(this);

        Order orderInfo = getArguments().getParcelable(ORDER_KEY);
        mUserActionListener.syncData(orderInfo);
        setTitle("Table list");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tables, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpList();
    }

    private void setUpList() {
        int numOfColumn = getResources().getInteger(R.integer.numOfColumn);

        mLayoutManager = new GridLayoutManager(getActivity(), numOfColumn);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mTableListAdapter);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(numOfColumn,
                spacingInPixels, true));
    }

    @Override
    public void displayTableAvailability(List<Table> tables) {
        mTableListAdapter.setTableList(tables);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mUserActionListener.stopSyncData();
    }

    @Override
    public void onTableClick(Table table) {
        if (table.isAvailable()) {
            Order orderInfo = getArguments().getParcelable(ORDER_KEY);
            orderInfo.setTableId(table.getId());
            Intent intent = DishActivity.makeIntent(getActivity(), orderInfo);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Table not available", Toast.LENGTH_SHORT).show();
        }
    }
}
