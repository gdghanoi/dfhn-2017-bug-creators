package bugcreators.com.bookable.screens.tables;

import android.content.Intent;

import com.google.firebase.auth.FirebaseUser;

import java.util.List;
import java.util.Map;

import bugcreators.com.bookable.commons.GenericViewInterface;
import bugcreators.com.bookable.model.Table.Table;
import bugcreators.com.bookable.model.order.Order;

/**
 * Created by Dandoh on 4/9/16.
 */
public interface TablesContract {


    interface View extends GenericViewInterface {
        void displayTableAvailability(List<Table> tables);
    }

    interface UserActionListener {
        void syncData(Order orderInfo);
        void stopSyncData();
    }
}
