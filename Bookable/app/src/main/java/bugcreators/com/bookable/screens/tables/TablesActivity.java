package bugcreators.com.bookable.screens.tables;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericActivity;
import bugcreators.com.bookable.model.order.Order;


public class TablesActivity extends GenericActivity {
    private static final String ORDER_KEY = "course id key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        if (savedInstanceState == null) {
            Order order = getIntent().getParcelableExtra(ORDER_KEY);
            initFragment(TablesFragment.getInstance(order));
        }
    }

    public static Intent makeIntent(Context context, Order order) {
        return new Intent(context, TablesActivity.class)
                .putExtra(ORDER_KEY, order);
    }
}
