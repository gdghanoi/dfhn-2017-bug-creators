package bugcreators.com.bookable.screens.time;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericActivity;
import bugcreators.com.bookable.screens.login.LoginFragment;

/**
 * Created by AnhVu on 11/18/17.
 */

public class TimeActivity extends GenericActivity{

    public static Intent getIntent(Context context) {
        return new Intent(context, TimeActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        if (savedInstanceState == null)
            initFragment(TimeFragment.getInstance());
    }
}
