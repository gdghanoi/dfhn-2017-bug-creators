package bugcreators.com.bookable.screens.login;

import android.content.Intent;

import com.google.firebase.auth.FirebaseUser;

import bugcreators.com.bookable.commons.GenericViewInterface;

/**
 * Created by Dandoh on 4/9/16.
 */
public interface LoginContract {


    interface View extends GenericViewInterface {
        void loginSuccess(FirebaseUser user);
        void loginFailure(String message);

    }

    interface UserActionListener {
        void loginGoogle();
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }
}
