package bugcreators.com.bookable.screens.time;

import bugcreators.com.bookable.commons.GenericViewInterface;

/**
 * Created by AnhVu on 11/18/17.
 */

public interface TimeContract {

    interface View extends GenericViewInterface {

    }

    interface UserActionListener {
    }

}
