package bugcreators.com.bookable.screens.dishes;

import java.util.List;

import bugcreators.com.bookable.commons.GenericViewInterface;
import bugcreators.com.bookable.model.dish.Dish;
import bugcreators.com.bookable.model.order.Order;

/**
 * Created by AnhVu on 11/18/17.
 */

public interface DishContract {
    interface View extends GenericViewInterface {
        void displayDishList(List<Dish> dishes);
        void afterInsert();
    }

    interface UserActionListener {
        void syncData();
        void stopSyncData();
        void insertOrder(Order order);
    }
}
