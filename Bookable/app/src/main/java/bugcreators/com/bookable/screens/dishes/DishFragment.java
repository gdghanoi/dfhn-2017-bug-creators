package bugcreators.com.bookable.screens.dishes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import bugcreators.com.bookable.R;
import bugcreators.com.bookable.commons.GenericRetainedFragment;
import bugcreators.com.bookable.commons.GenericRetainedToolbarFragment;
import bugcreators.com.bookable.customview.GridSpacingItemDecoration;
import bugcreators.com.bookable.model.dish.Dish;
import bugcreators.com.bookable.model.order.Order;
import bugcreators.com.bookable.screens.tables.TablesActivity;
import bugcreators.com.bookable.screens.tables.TablesFragment;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by AnhVu on 11/18/17.
 */

public class DishFragment extends GenericRetainedToolbarFragment implements DishContract.View {
    private DishContract.UserActionListener mUserActionListener;
    DishListAdapter mDishListAdapter;

    @BindView(R.id.list_table)
    RecyclerView mRecyclerView;

    private static final String ORDER_KEY = "Order key";
    public static Fragment getInstance(Order order) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ORDER_KEY, order);
        Fragment fragment = new DishFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserActionListener = new DishPresenter(this.getActivity(), this);
        mDishListAdapter = new DishListAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pickdishes, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpList();
        mUserActionListener.syncData();
    }

    private void setUpList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mDishListAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUserActionListener.stopSyncData();
    }

    @Override
    public void displayDishList(List<Dish> dishes) {
        mDishListAdapter.setDishList(dishes);
    }

    @Override
    public void afterInsert() {
        Toast.makeText(getActivity(), "Order sucessfully", Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_continue)
    void onContinueClick() {
        Order orderInfo = getArguments().getParcelable(ORDER_KEY);
        mUserActionListener.insertOrder(orderInfo);

    }
}
