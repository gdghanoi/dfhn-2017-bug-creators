package bugcreators.com.bookable.screens.dishes;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bugcreators.com.bookable.BookableApplication;
import bugcreators.com.bookable.R;
import bugcreators.com.bookable.model.dish.Dish;

/**
 * Created by AnhVu on 11/18/17.
 */

public class DishListAdapter extends RecyclerView.Adapter<DishListAdapter.ViewHolder>  {

    private static final String TAG = "DishListAdapter";
    private List<Dish> mDishes;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public DishListAdapter() {
        mDishes = new ArrayList<>();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DishListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
//         create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dish, parent, false);
        // set the view's size, margins, paddings and layout parameters
        DishListAdapter.ViewHolder vh = new DishListAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(DishListAdapter.ViewHolder holder, final int position) {
        final Dish dish = mDishes.get(position);

        View v = holder.itemView;
        TextView name = (TextView) v.findViewById(R.id.tv_dish_name);
        TextView amount = (TextView) v.findViewById(R.id.tv_dish_num);
        ImageView imageView = (ImageView) v.findViewById(R.id.tv_dish_img);
        ImageView increaseView = (ImageView) v.findViewById(R.id.increase);
        ImageView decreaseView = (ImageView) v.findViewById(R.id.decrease);

        Picasso.with(BookableApplication.getAppContext())
                .load(dish.getImage())
                .into(imageView);
        name.setText(dish.getName());
        amount.setText(dish.getAmount() + "");

        increaseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dish.setAmount(dish.getAmount() + 1);
                DishListAdapter.this.notifyDataSetChanged();
            }
        });
        decreaseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dish.setAmount(dish.getAmount() - 1);
                DishListAdapter.this.notifyDataSetChanged();
            }
        });



    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDishes.size();
    }


    public void setDishList(List<Dish> dishes) {
        Log.i("HAHA", dishes.size() + " ");
        this.mDishes = dishes;
        notifyDataSetChanged();
    }
}
