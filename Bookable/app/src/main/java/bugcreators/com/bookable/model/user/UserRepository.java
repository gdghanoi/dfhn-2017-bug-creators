package bugcreators.com.bookable.model.user;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Dandoh on 11/18/17.
 */

public class UserRepository {
    private static UserRepository mInstance;

    public static UserRepository getInstance() {
        if (mInstance == null) {
            mInstance = new UserRepository();
        }

        return mInstance;
    }

    DatabaseReference mDatabaseReference;

    public UserRepository() {
        this.mDatabaseReference = FirebaseDatabase.getInstance()
                .getReference();
    }

    public void insertUser(User user) {
        mDatabaseReference.child("users").child(user.getUuid()).setValue(user);
    }
}
